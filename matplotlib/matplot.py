import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

x = np.linspace(0, 10, 100)

fig = plt.figure()
ax = plt.axes()

ax.plot(x, np.sin(x), 'b-', label='sin')
ax.plot(x, np.cos(x), 'r-', label='cos')

leg = ax.legend()

x = 20 * np.random.randn(200) + 100
y = x + (10 * np.random.randn(200) + 50)

colors = np.random.randn(200)
sizes = np.random.randn(200) * 300

fig = plt.figure()
ax = plt.axes()

s = ax.scatter(x, y, marker='o')
ax.set_title("random data")
ax.set_xlabel("random x-values")
ax.set_ylabel("random y-values")

us = pd.read_csv("https://raw.githubusercontent.com/nytimes/covid-19-data/master/us.csv")

cases = us["cases"]
days = np.arange(len(cases))
fig = plt.figure()
ax = plt.axes()

ax.plot(days, cases, 'b-', label='cases')
plt.show()


