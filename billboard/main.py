import pandas as pd
import datetime

songscsv = pd.read_csv('data.csv')
songscsv['WeekID'] = songscsv['WeekID'].astype(str)

songscsv['Song'] = songscsv['Song'].astype(str)
songscsv['Performer'] = songscsv['Performer'].astype(str)

curdate = datetime.datetime(1964,9,12)
findate = datetime.datetime(1965,6,12)
dates = []
while True:
    dates.append(curdate)
    curdate += datetime.timedelta(days=7)
    if curdate == findate:
        break
newcsv = pd.DataFrame(columns = dates)
songs = []
for date, songs in newcsv.iteritems():
    searchparam = date.strftime('%-m/%-d/%Y')
    #print(searchparam)
    #print(songscsv.dtypes)
    datehits = songscsv.loc[songscsv['WeekID'] == searchparam]
    datehits = datehits.reset_index()
    datehits = datehits.sort_values(by=['Week Position'])
    datehits = datehits.reset_index()
    songs.append(datehits['Song'])
    #print(songs)
    newcsv[date] = datehits['Song'].astype(str) + " by " + datehits['Performer'].astype(str)

print(newcsv)
print(songs)
newcsv.to_csv('out.csv')